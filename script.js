'use strict';
/* Ответы на теоритические вопросы: 
1. Document Object Model (DOM) - структура документа, определяет как эта структура доступна 
из программ для изменения структуры, стилей и тд.
2. innerHTML показывает информацию по одному элементу, а innerText показывает все
содержимое между отк. и закр. тегом.
3. Сейчас используются 2 способа Document.querySelector() более распространенный, и 
document.getElementById() менее распространенный. */

const paragraphs = document.querySelectorAll('p');
/* for (let elem of paragraphs) {
    console.log(elem);
    elem.style.backgroundColor = '#ff0000';    как вариант
} */

paragraphs.forEach(function (elem) {
    elem.style.backgroundColor = '#ff0000';
});

const optionsList = document.querySelector('#optionsList');
console.log(optionsList);

const parentElem = optionsList.parentNode;
console.log(parentElem);

const childElem = optionsList.children; // только узы (только для чтения)
// const childElem = optionsList.childNodes - включает текстовые устройства и комментарии (только для чтения)
for (let child of childElem) {
    console.log(child);
}

const addElem = document.createElement('p');
addElem.textContent = 'This is a paragraph';
addElem.classList.add('testParagraph');
console.log(addElem);


const findElem = document.querySelector('.main-header');
const headerElem = findElem.children;
for (let childs of headerElem) {
    console.log(childs);
}
const firstElem = document.querySelector('.container.main-header-content');
firstElem.classList.add('nav-item');
const secondElem = document.querySelector('.container.d-flex');
secondElem.classList.add('nav-item');
console.log(firstElem, secondElem);


const findSections = document.querySelectorAll('.section-title');
findSections.forEach(item => item.classList.remove('.section-title'));
console.log(findSections);